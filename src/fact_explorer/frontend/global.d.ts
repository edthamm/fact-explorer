export declare global {
  type EventFieldLensConfig = {
    fields: (string | RegExp)[];
    events: { type: string; value: (eventData: unknown) => string }[];
  };

  interface Window {
    /** Runtime configuration. */
    FACT_EXPLORER: {
      eventFieldLens?: EventFieldLensConfig[];
      disableCodeCompletionForHeader?: boolean;
      disableDocumentationForPayload?: boolean;
    };
  }
}
