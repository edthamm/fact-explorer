type Options = {
  onFocusAggId?: (aggId: string) => void;
  onMergeAggId?: (aggId: string) => void;
};

export const options: Options = {};
