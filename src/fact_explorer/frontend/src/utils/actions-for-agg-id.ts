// small word of warning - not the most beautiful code ever, sorry
// first goals is to get everything working
import { iterator, parse } from '@humanwhocodes/momoa';
import { Monaco } from '@monaco-editor/react';
import { options } from './custom-monaco-options';

let lastHoverCacheKey = '';
let cachedAggIds: any[] = [];

export function actionsForAggId(monaco: Monaco) {
  const focusCommandId = 'fact-explorer.focusAggId';
  const mergeCommandId = 'fact-explorer.mergeAggId';

  monaco.editor.registerCommand(focusCommandId, (ctx, args) => {
    if (options.onFocusAggId) {
      options.onFocusAggId?.(args.aggId);
    } else {
      alert(`Command "${focusCommandId}" is not handled correctly.`);
    }
  });

  monaco.editor.registerCommand(mergeCommandId, (ctx, args) => {
    if (options.onMergeAggId) {
      options.onMergeAggId?.(args.aggId);
    } else {
      alert(`Command "${focusCommandId}" is not handled correctly.`);
    }
  });

  monaco.languages.registerHoverProvider('json', {
    provideHover(model, position) {
      if (model.uri.path !== '/result/search.json') return;

      // get agg ids (from cache if possible)
      const cacheKey = `${model.id}-${model.getVersionId()}`;
      if (cacheKey !== lastHoverCacheKey) {
        const ast = parse(model.getValue());
        const aggIds: any[] = [];

        for (const { node, parent, phase } of iterator(
          ast,
          ({ phase }: any) => phase === 'enter'
        )) {
          if (
            node.type === 'Member' &&
            node.name.value === 'aggIds' &&
            node.value.type === 'Array'
          ) {
            const ids = node.value.elements.filter(
              (element: any) => element.type === 'String'
            );
            aggIds.push(...ids);
          }
        }
        lastHoverCacheKey = cacheKey;
        cachedAggIds = aggIds;
      }

      // search agg id and provide hover
      const aggId = cachedAggIds.find(
        (aggId) =>
          aggId.loc.start.line === position.lineNumber &&
          aggId.loc.end.line === position.lineNumber &&
          aggId.loc.start.column <= position.column &&
          aggId.loc.end.column >= position.column
      );
      if (!aggId) return null;

      const encodedArgs = encodeURIComponent(
        JSON.stringify({ aggId: aggId.value })
      );
      const focusLabel = 'Focus';
      const focusTooltip = 'Filter for this specific aggId.';
      const mergeLabel = 'Add to current filter';
      const mergeTooltip = 'Adds this specific aggId to the current filter.';

      return {
        range: new monaco.Range(
          aggId.loc.start.line,
          aggId.loc.start.column,
          aggId.loc.end.line,
          aggId.loc.end.column
        ),
        contents: [
          {
            value: `[${focusLabel}](command:${focusCommandId}?${encodedArgs} "${focusTooltip}") | [${mergeLabel}](command:${mergeCommandId}?${encodedArgs} "${mergeTooltip}")`,
            isTrusted: true,
          },
        ],
      };
    },
  });
}
