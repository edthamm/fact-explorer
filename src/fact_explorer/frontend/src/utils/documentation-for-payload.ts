// small word of warning - not the most beautiful code ever, sorry
// first goals is to get everything working
import { iterator, parse } from '@humanwhocodes/momoa';
import { Monaco } from '@monaco-editor/react';
import { limitFetch } from './limit-fetch';

let lastHoverCacheKey = '';
let cachedPayloads: any[] = [];
const schemaDocumentationCache: Record<string, any> = {};

export function documentationForPayload(monaco: Monaco) {
  monaco.languages.registerHoverProvider('json', {
    provideHover(model, position) {
      if (!model.uri.path.startsWith('/result/')) return;

      // get agg ids (from cache if possible)
      const cacheKey = `${model.id}-${model.getVersionId()}`;
      if (cacheKey !== lastHoverCacheKey) {
        const ast = parse(model.getValue());
        const payloads: any[] = [];

        for (const { node, parent, phase } of iterator(
          ast,
          ({ phase }: any) => phase === 'enter'
        )) {
          if (
            node.type === 'Object' &&
            node.members.length === 2 &&
            node.members[0].name.value === 'header' &&
            node.members[1].name.value === 'payload'
          ) {
            const [headerNode, payloadNode] = node.members;
            const ns = headerNode.value.members.find(
              (member: any) => member.name.value === 'ns'
            ).value.value;
            const type = headerNode.value.members.find(
              (member: any) => member.name.value === 'type'
            ).value.value;
            const version = headerNode.value.members.find(
              (member: any) => member.name.value === 'version'
            ).value.value;
            payloads.push({ node: payloadNode, ns, type, version });
          }
        }
        lastHoverCacheKey = cacheKey;
        cachedPayloads = payloads;
      }

      // search payload and provide hover
      const payload = cachedPayloads.find(
        ({ node }) =>
          (node.loc.start.line < position.lineNumber ||
            (node.loc.start.line === position.lineNumber &&
              node.loc.start.column <= position.column)) &&
          (node.loc.end.line > position.lineNumber ||
            (node.loc.end.line === position.lineNumber &&
              node.loc.end.column >= position.column))
      );
      if (!payload) return null;

      // possible future enhancement - cancel fetch, if no longer needed
      return getSchema(payload)
        .then((markdown) => {
          return {
            range: new monaco.Range(
              position.lineNumber,
              position.column,
              position.lineNumber,
              position.column
            ),
            contents: [
              {
                value: markdown,
                isTrusted: true,
              },
            ],
          };
        })
        .catch((error) => {
          console.error('Could not fetch schema from registry.');
          console.error(error);
          return null;
        });
    },
  });
}

async function getSchema({
  ns,
  type,
  version,
}: {
  ns: string;
  type: string;
  version: number;
}) {
  const params = `namespace=${ns}&type=${type}&version=${version}`;

  // use cache if possible
  if (schemaDocumentationCache[params]) return schemaDocumentationCache[params];

  const url = `/api/registry/schema?${params}`;
  const res = await limitFetch(url, () => fetch(url));

  if (!res.ok) throw await res.clone().json();

  const schema = await res.clone().json();
  let markdown = '';

  function forEachSchema(schema: any, depth = 0) {
    // fallback to 'title' to support older schema registry versions
    const description = schema.description ?? schema.title;
    if (depth === 0 && description) markdown += description + `\n\n`;

    const props = schema.properties || schema.items.properties;
    if (props) {
      Object.keys(props).forEach((key) => {
        const prop = props[key];
        const isRequired = schema.required?.find(
          (item: string) => item === key
        );
        markdown +=
          (depth !== 0 ? ' '.repeat(depth * 2) + '- ' : '- ') +
          '**' +
          key +
          '**' +
          (isRequired ? ' _(required)_' : '') +
          `: \`{${prop.type}}\` ${
            // fallback to 'title' to support older schema registry versions
            prop.description ?? prop.title ?? '_no description_'
          }\n\n`;
        if (prop.type === 'object') forEachSchema(prop, depth + 1);
        if (prop.type === 'array') forEachSchema(prop, depth + 1);
      });
    }
  }

  forEachSchema(schema);

  schemaDocumentationCache[params] = markdown;

  return markdown;
}
