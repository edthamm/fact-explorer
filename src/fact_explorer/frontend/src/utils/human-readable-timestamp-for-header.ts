// small word of warning - not the most beautiful code ever, sorry
// first goals is to get everything working
import { iterator, parse } from '@humanwhocodes/momoa';
import { Monaco } from '@monaco-editor/react';
import { formatISO } from 'date-fns';

let lastLensCacheKey = '';
let cachedTimestamps: any[] = [];

export function humanReadableTimestampForHeader(monaco: Monaco) {
  monaco.languages.registerCodeLensProvider('json', {
    provideCodeLenses(model, token) {
      if (!model.uri.path.startsWith('/result/')) return;

      // get agg ids (from cache if possible)
      const cacheKey = `${model.id}-${model.getVersionId()}`;
      if (cacheKey !== lastLensCacheKey) {
        const ast = parse(model.getValue());
        const timestamps: any[] = [];
        for (const { node, parent, phase } of iterator(
          ast,
          ({ phase }: any) => phase === 'enter'
        )) {
          if (
            node.type === 'Member' &&
            node.name.value === '_ts' &&
            node.value.type === 'Number'
          ) {
            timestamps.push({ node, title: formatISO(node.value.value) });
          }
        }
        lastLensCacheKey = cacheKey;
        cachedTimestamps = timestamps;
      }

      return {
        lenses: cachedTimestamps.map((ts) => ({
          range: new monaco.Range(
            ts.node.loc.start.line,
            ts.node.loc.start.column,
            ts.node.loc.end.line,
            ts.node.loc.end.column
          ),
          command: {
            id: '',
            title: ts.title,
          },
        })),
        dispose: () => {},
      };
    },
    resolveCodeLens(model, codeLens, token) {
      return codeLens;
    },
  });
}
