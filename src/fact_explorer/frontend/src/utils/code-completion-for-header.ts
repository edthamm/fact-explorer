// small word of warning - not the most beautiful code ever, sorry
// first goals is to get everything working
import { Monaco } from '@monaco-editor/react';
import { limitFetch } from './limit-fetch';

type NamespaceData = {
  title: string;
  types: { name: string; version: number }[];
};

export async function codeCompletionForHeader(monaco: Monaco) {
  try {
    const url = `/api/registry/namespaces?with-types=true`;
    const res = await limitFetch(url, () => fetch(url));

    if (!res.ok) throw await res.clone().json();

    const data: Record<string, NamespaceData> = await res.clone().json();

    const namespaces = Object.keys(data);

    const anyOf: any[] = [];

    namespaces.forEach((namespace) => {
      const nsData = data[namespace];
      nsData.types.forEach((type) => {
        anyOf.push({
          type: 'object',
          properties: {
            ns: { description: nsData.title, enum: [namespace] },
            type: { enum: [type.name] },
            version: { enum: [type.version] },
          },
        });
      });
    });

    monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
      validate: true,
      trailingCommas: 'ignore',
      allowComments: true,
      schemas: [
        {
          uri: 'http://fact-explorer/header-schema.json',
          fileMatch: ['header.json'],
          schema: {
            type: 'object',
            properties: {
              id: {
                type: 'string',
              },
              aggIds: {
                type: 'array',
                items: {
                  type: 'string',
                },
              },
              ns: { enum: namespaces },
              type: {
                type: 'string',
              },
              version: {
                type: 'integer',
              },
            },
            anyOf,
          },
        },
      ],
    });
  } catch (error) {
    console.error('Could not fetch namespaces with types from registry.');
    console.error(error);
  }
}
