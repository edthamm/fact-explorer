import { yupResolver } from '@hookform/resolvers/yup';
import type { NextPage } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import * as queryString from 'query-string';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { Card } from '../components/card';
import { Form } from '../components/form';
import { Label } from '../components/label';
import { Result } from '../components/result';
import { Title } from '../components/title';
import { useFetch } from '../hooks/use-fetch';
import { useParams } from '../hooks/use-params';
import { useSubmit } from '../hooks/use-submit';
import { FactOut } from '../types/types';
import { isSameQuery } from '../utils/is-same-query';

const schema = yup
  .object({
    minutes: yup
      .number()
      .transform((value, originalValue) =>
        originalValue === '' ? undefined : value
      )
      .typeError('minutes must be a number')
      .required()
      .integer()
      .min(1)
      .max(60),
    decrypt: yup.boolean().defined(),
  })
  .required();

type FormValues = yup.Asserts<typeof schema>;

const defaultFormValues: FormValues = {
  minutes: 15,
  decrypt: false,
};

const Home: NextPage = () => {
  const params = useParams(defaultFormValues);

  function executeLastMinutes(params: FormValues) {
    const query = queryString.stringify({
      minutes: params.minutes,
      decrypt: params.decrypt,
    });
    lastMinutes.execute(`/api/last?${query}`, {
      headers: { Accept: 'application/json' },
    });
  }

  useEffect(() => {
    executeLastMinutes(params);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>({
    resolver: yupResolver(schema),
    defaultValues: params,
  });

  const lastMinutes = useFetch<FactOut[]>();
  const submit = useSubmit();
  const router = useRouter();

  const onSubmit = handleSubmit(
    (values) => {
      if (isSameQuery(router.query, values)) executeLastMinutes(values);
      else
        router.push({ pathname: '/', query: values }, undefined, {
          scroll: false,
        });
    },
    (errors) => {
      submit.onError();
    }
  );
  const paddingLeft = { paddingLeft: '26px' };
  const height = { height: '28px' }; // sum of lineHeigt + padding from <EditorInput />
  return (
    <>
      <Head>
        <title>Last Minutes</title>
      </Head>

      <Title>Retrieve Events from Last Minutes</Title>

      <Card>
        <Form onSubmit={onSubmit}>
          <div>
            <Label htmlFor="minutes">Minutes</Label>
            <input
              id="minutes"
              type="number"
              style={{ ...height, ...paddingLeft }}
              className="focus:ring-blue-500 focus:border-blue-500 w-full shadow-sm border border-gray-300 rounded-md p-1"
              {...register('minutes')}
            />
            {errors.minutes && (
              <p className="text-sm text-red-500">⚠ {errors.minutes.message}</p>
            )}
          </div>

          <label>
            <input type="checkbox" {...register('decrypt')} /> Decrypt Result
          </label>

          <div className="text-right">
            <button {...submit.props}>Get Events</button>
          </div>
        </Form>

        {process.browser && router.isReady && (
          <Result request={lastMinutes} path="result/last-minutes.json" />
        )}
      </Card>
    </>
  );
};

export default Home;
