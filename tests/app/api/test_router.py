from fastapi.testclient import TestClient
import pytest
from os import environ as env


@pytest.fixture(scope="function")
def client(fe_dir, mock_decryptor, app_config):
    from fact_explorer.app.main import app

    return TestClient(app)


@pytest.fixture(scope="function")
def no_cache_prewarm():
    env["DISABLE_CACHE_PREWARM"] = "true"

    yield

    del env["DISABLE_CACHE_PREWARM"]


@pytest.mark.big
def test_features(client, no_cache_prewarm):
    with client as client:
        res = client.get("/api/features")

    assert res.status_code == 200
    assert res.json() == {
        "cryptoshred": True,
        "schema_registry": True,
        "payload_queries": True,
    }
