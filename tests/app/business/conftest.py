import pytest


@pytest.fixture(scope="function")
def mock_registry(mocker):
    mock = mocker.patch(
        "schema_registry.registries.HttpRegistry.EnrichingHttpSchemaRegistry"
    )

    return mock
