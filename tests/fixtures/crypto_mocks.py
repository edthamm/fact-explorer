import pytest


@pytest.fixture(scope="function")
def mock_decryptor(mocker):
    patched_function = mocker.patch(
        "cryptoshred.asynchronous.convenience.find_and_decrypt"
    )
    patched_backend = mocker.patch("cryptoshred.backends.DynamoDbSsmBackend")

    return patched_function, patched_backend
