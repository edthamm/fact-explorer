Quickstart Guide
================

To run this tool along side your factcast, you will need to perform a few simple steps.
And you will need to meet some prerequisites.
This article will describe on how to set this up with containers in AWS.
If your scenario is different, remember this is just a uvicorn app.
No fancy stuff going on.

Prerequisites
-------------

- You will need access to the factcast database


Steps to get Up and Running
---------------------------

- Build a container
- Configure the application


Building a container
--------------------

All you need is a container running python >= 3.9 with the package for this app installed.

The easiest way to get there is to create a Dockerfile with this content:

.. code::

  FROM python:3.9-bullseye

  RUN pip install poetry fact_explorer

  CMD [ "uvicorn","fact_explorer.app.main:app", "--port", "8080", "--host", "0.0.0.0" ]

Take this container and run it somewhere that can reach the postgres database of your event store.

Be aware that this will get you the most basic setup you will need to install additional packages like
``cryptoschred`` and ``schema-registry`` if you want to be able to use convenience features like
auto-complete of input, help texts or the ability to decrypt crypto containers in your events.

Configuration
-------------

You have the following options you can set either through the environment, or through
a configuration file called ``default.env`` in the ``~/.fact_explorer`` directory of the user
running Fact Explorer.

- DATABASE_PASSWORD: (required) the database password to use with the factcast database
- CRYPTOSHRED_INIT_VECTOR_PATH: (optional) the location of the crytposhred i.v. (required if you have the optional cryptoshred dependency installed)
- SCHEMA_REGISTRY_URL: (optional) the url of your schema registry (required if you have the optional schema-registry dependency installed)
- FACT_EXPLORER_LOG_LEVEL: (optional) defaults to WARNING
- DATABASE_USER: (optional) defaults to rdssystem
- DATABASE_HOST: (optional) defaults to localhost
- DATABASE_PORT: (optional) defaults to 5432
- DATABASE_NAME: (optional) defaults to postgres

It is highly recommended to use a read only user with Fact Explorer.

Check that it Works
-------------------

Go to the URL of the container or the loadbalancer in front of it and you should see the welcome page of the
Fact Explorer.
